<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Register</title>
</head>

<body>
    <main>
        <form action="register.php" method="post">
            <div class="container">
                <?php
                // define variables to empty values
                $name = $birth = $address = "";
                $gender = $faculty = array();

                // fields validation
                if ($_SERVER["REQUEST_METHOD"] == "POST") {

                    // name validation
                    if (empty($_POST["name"])) {
                        echo "<font color='red'>Hãy nhập tên.</font> </br>";
                    }

                    // gender validation
                    if (empty($_POST["gender"])) {
                        echo "<font color='red'>Hãy chọn giới tính.</font> </br>";
                    }

                    // faculty validation
                    if ($_POST["faculty"] == "None") {
                        echo "<font color='red'>Hãy chọn phân khoa.</font> </br>";
                    }

                    // birth validation
                    if (empty($_POST["birth"])) {
                        echo "<font color='red'>Hãy nhập ngày sinh.</font> </br>";
                    } else {
                        $birth_arr  = explode('/', $_POST["birth"]);
                        if (count($birth_arr) != 3 || !checkdate($birth_arr[1], $birth_arr[0], $birth_arr[2])) {
                            echo "<font color='red'>Hãy nhập ngày sinh đúng định dạng.</font> </br>";
                        }
                    }
                }
                ?>
                <div>
                    <label for="name">Họ và tên<span class="required">*</span></label>
                    <input type="text" class="input" name="name">
                </div>

                <div>
                    <label for="gender">Giới tính<span class="required">*</span></label>
                    <?php
                    $genders = array("Nam", "Nữ");
                    for ($i = 0; $i < count($genders); $i++) {
                        echo "<input type='radio' id=$i name='gender' value=$genders[$i]>$genders[$i] ";
                    }
                    ?>
                </div>

                <div>
                    <label for="faculty">Phân khoa<span class="required">*</span></label>
                    <select name='faculty'>
                        <?php
                        $faculties = array(
                            "None" => "",
                            "MAT" => "Khoa học máy tính",
                            "KDL" => "Khoa học vật liệu",
                        );
                        foreach ($faculties as $key => $value) {
                            echo "<option value=$key>$value</option>";
                        }
                        ?>
                    </select>
                </div>

                <div>
                    <label for="birth">Ngày sinh<span class="required">*</span></label>
                    <input type="text" class="input" name="birth" placeholder="dd/mm/yyyy">
                    <!-- <input data-date-format='DD-MM-YYYY' type='date' class='date' name='birth' /><br> -->
                </div>

                <div>
                    <label for="address">Địa chỉ</label>
                    <input type="text" class="input" name="address">
                </div>

                <button type="submit" class="button" name="register">Đăng ký</button>
        </form>
    </main>
</body>

</html>